import 'dart:developer';
import 'dart:typed_data';

import 'package:calculator_rx_dart/core/disposer.dart';
import 'package:calculator_rx_dart/models/note_model.dart';
import 'package:flutter/services.dart';
import 'package:rxdart/rxdart.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';

class NoteDatabaseViewModel extends Disposer {
  final BehaviorSubject<List<NoteModel>> _dataNoteObs = BehaviorSubject<List<NoteModel>>();
  final int _version = 1;
  final String _dbName = 'Notes.db';

  BehaviorSubject get dataNoteObs => _dataNoteObs;

  Future<void> initialData() async {
    _dataNoteObs.add([]);
    await getAllNotes();
  }

  Future<Database> _getDB() async {
    final fullPath = join(await getDatabasesPath(), _dbName);
    return openDatabase(
      fullPath,
      onCreate: (db, version) async => await db.execute("CREATE TABLE Note(id INTEGER PRIMARY KEY, title TEXT NOT NULL, description TEXT NOT NULL, price REAL NOT NULL, image BLOB);"),
      // onCreate: (db, version) async => await db.execute("CREATE TABLE Note(""id INTEGER PRIMARY KEY,""title TEXT NOT NULL,""title TEXT NOT NULL"");"),
      version: _version,
    );
  }

  Future<void> addNote(NoteModel note) async {
    final db = await _getDB();
    await db.insert('Note', note.toJson(), conflictAlgorithm: ConflictAlgorithm.replace);
    await getAllNotes();

    /// Ex.
    // final db = await openMyDatabase();
    // await db.insert(
    //   'my_table',
    //   {
    //     'column1': 'value1',
    //     'column2': 42,
    //   },
    // );
  }

  Future<void> updateNote(NoteModel note) async {
    final db = await _getDB();
    await db.update('Note', note.toJson(), where: 'id = ?', whereArgs: [note.id], conflictAlgorithm: ConflictAlgorithm.replace);
    await getAllNotes();
  }

  Future<void> deleteNote(NoteModel note) async {
    final db = await _getDB();
    await db.delete('Note', where: 'id = ?', whereArgs: [note.id]);
    await getAllNotes();
  }

  Future<void> getAllNotes() async {
    final db = await _getDB();
    final List<Map<String, dynamic>> maps = await db.query('Note');
    if (maps.isEmpty) {
      _dataNoteObs.sink.add([]);
    } else {
      log('length database : ${maps.length}');
      _dataNoteObs.sink.add(
        List.generate(maps.length, (index) => NoteModel.fromJson(maps[index])),
      );
    }
  }

  // final image =


  Future<Uint8List> imageAssetToUint8List(String assetPath) async {
    ByteData byteData = await rootBundle.load(assetPath);
    Uint8List bytes = byteData.buffer.asUint8List();
    return bytes;
  }
  Future<Uint8List> imageToUint8List() async {
    Uint8List imageBytes = await imageAssetToUint8List('assets/images.png');
    // log(imageBytes.toString());
    return imageBytes;

  }

  @override
  void dispose() {
    _dataNoteObs.close();
    // TODO: implement dispose
  }
}
