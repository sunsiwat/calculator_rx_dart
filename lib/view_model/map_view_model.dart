import 'dart:async';
import 'dart:developer';

import 'package:calculator_rx_dart/core/disposer.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';
import 'package:rxdart/rxdart.dart';

class MapViewModel extends Disposer {
  final BehaviorSubject<CameraPosition> _dataCamObs = BehaviorSubject<CameraPosition>();
  final BehaviorSubject<Set<Marker>> _dataMarkerObs = BehaviorSubject<Set<Marker>>.seeded(<Marker>{});

  final Completer<GoogleMapController> _controller = Completer();
  late LocationData? currentLocation;
  int number = 0;
  BitmapDescriptor markerIcon = BitmapDescriptor.defaultMarker;
  List<Marker> markerList = [];
  BitmapDescriptor myIcon = BitmapDescriptor.defaultMarker;

  BehaviorSubject get dataCamObs => _dataCamObs;
  BehaviorSubject get dataMarkerObs => _dataMarkerObs;
  Completer<GoogleMapController> get controller => _controller;

  void setInitialData() async {
    final location = await getCurrentPosition();
    customIcon();
    await Future.delayed(const Duration(seconds: 1));
    addMarker(LatLng(location.latitude, location.longitude));
    _dataCamObs.sink.add(CameraPosition(target: LatLng(location.latitude, location.longitude), zoom: 16));
  }

  Future<Position> getCurrentPosition() async {
    bool serviceEnabled;
    LocationPermission permission;

    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      return Future.error('Location services are disabled.');
    }

    log('request permission');
    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        return Future.error('Location permissions are denied');
      }
    }

    if (permission == LocationPermission.deniedForever) {
      return Future.error('Location permissions are permanently denied, we cannot request permissions.');
    }
    return await Geolocator.getCurrentPosition();
  }

  void customIcon() async {
    // BitmapDescriptor.fromAssetImage(const ImageConfiguration(size: Size(1, 1)), 'assets/marker1.png').then((onValue) {
    //   myIcon = onValue;
    // });

    String imgUrl = "https://www.fluttercampus.com/img/car.png";
    Uint8List imgBytes = (await NetworkAssetBundle(Uri.parse(imgUrl)).load(imgUrl)).buffer.asUint8List();
    myIcon = BitmapDescriptor.fromBytes(imgBytes);
  }

  Future goToMyLocation() async {
    final GoogleMapController controller = await _controller.future;
    final location = await getCurrentPosition();
    controller.animateCamera(CameraUpdate.newCameraPosition(CameraPosition(
      target: LatLng(
        location.latitude,
        location.longitude,
      ),
      zoom: 16,
    )));
  }

  Future goToAirport() async {
    log('airport');
    final GoogleMapController controller = await _controller.future;
    controller.animateCamera(CameraUpdate.newLatLngZoom(const LatLng(13.6920043, 100.7499237), 16));
  }

  Future zoomOutBangkok() async {
    log('zoomOut');
    final GoogleMapController controller = await _controller.future;
    controller.animateCamera(CameraUpdate.newLatLngZoom(const LatLng(13.6846021, 100.5883304), 10));
  }

  void addMarker(LatLng latLng) {
    number += 1;
    final Set<Marker> markerSet = _dataMarkerObs.value;
    final Marker marker = Marker(
      markerId: MarkerId('$number'),
      position: latLng,
      infoWindow: InfoWindow(
        title: 'Mark No.$number',
        snippet: '$latLng',
      ),
      icon: myIcon,
    );
    markerSet.add(marker);
    _dataMarkerObs.sink.add(markerSet);
  }

  @override
  void dispose() {
    _dataCamObs.close();
    _dataMarkerObs.close();
    // TODO: implement dispose
  }
}
