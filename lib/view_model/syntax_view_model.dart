import 'dart:developer';

import 'package:calculator_rx_dart/core/disposer.dart';
import 'package:calculator_rx_dart/utils/constants.dart';
import 'package:rxdart/rxdart.dart';

class SyntaxViewModel extends Disposer {
  final BehaviorSubject<String> _dataStrObs = BehaviorSubject<String>();
  String symbolicSentence = '';
  RegExp regExp = RegExp(r'^([+-]?\d+\.?\d*)([+\-*/])([+-]?\d+\.?\d*)'); //(\.\d+)?
  // RegExp regExp = RegExp(r'(\d+)([+\-*/])(\d+)');

  BehaviorSubject get dataStrObs => _dataStrObs;

  void setInitialData() {
    // _dataStrObs.stream();
    _dataStrObs.sink.add('');
  }

  void syntax(String char) {
    symbolicSentence = _dataStrObs.value;
    switch (char) {
      case '<--':
        if (symbolicSentence.isNotEmpty) {
          symbolicSentence = symbolicSentence.substring(0, symbolicSentence.length - 1);
        }
      case 'c':
        symbolicSentence = '';
      case '±':
      case '%':
        break;
      case '=':
        parseAndOperate(symbolicSentence);
        break;
      default:
        if (symbolicSentence.isNotEmpty) {
          if (operatorsConst.contains(char)) {
            // +-*/
            if (operatorsConst.contains(symbolicSentence[symbolicSentence.length - 1])) {
              symbolicSentence = symbolicSentence.substring(0, symbolicSentence.length - 1) + char;
            } else {
              // คำนวณค่ากให้ได้ค่าใหม่ก่อนที่จะใสาเครื่องวหมาย Ex 2+3 --> char = - --> 5-
              parseAndOperate(symbolicSentence);
              symbolicSentence = symbolicSentence + char;
            }
          } else {
            // 1234567890.
            symbolicSentence = symbolicSentence + char;
          }
        } else {
          if (numPadsConst.contains(char)) {
            symbolicSentence = symbolicSentence + char;
          }
        }
    }
    _dataStrObs.sink.add(symbolicSentence);
  }

  void parseAndOperate(String input) {
    Match? match = regExp.firstMatch(input);
    if (match != null) {
      // ดึงตัวเลขและเครื่องหมายจากกลุ่มที่จับได้
      double num1 = double.parse(match.group(1)!);
      String operator = match.group(2)!;
      double num2 = double.parse(match.group(3)!);

      double result;
      switch (operator) {
        case '+':
          result = num1 + num2;
          break;
        case '-':
          result = num1 - num2;
          break;
        case '*':
          result = num1 * num2;
          break;
        case '/':
          result = num1 / num2;
          break;
        default:
          throw UnsupportedError('Unsupported operator: $operator');
      }

      log('$num1 $operator $num2 = $result');
      symbolicSentence = result.toString();
    } else {
      log('Invalid format: $input');
    }
  }

  @override
  void dispose() {
    // _dataStrObs.close();
  }

}
