import 'dart:developer';

import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';

class FirebaseViewModel {
  final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();
  late final FirebaseMessaging _messaging;

  void setupFirebase() async {
    _messaging = FirebaseMessaging.instance;
    String? token = await _messaging.getToken();
    log(token.toString());

    NotificationSettings settings = await _messaging.requestPermission(
      alert: true,
      badge: true,
      provisional: false,
      sound: true,
    );

    if (settings.authorizationStatus == AuthorizationStatus.authorized) {
      var androidInit = const AndroidInitializationSettings('@mipmap/ic_launcher');
      var iosInit = const DarwinInitializationSettings(
        requestAlertPermission: true,
        requestBadgePermission: true,
        requestSoundPermission: true,
      );

      var initializationSettings = InitializationSettings(android: androidInit, iOS: iosInit);

      flutterLocalNotificationsPlugin.initialize(
        initializationSettings
      );

      FirebaseMessaging.onMessage.listen((RemoteMessage message) {
        log('receive message');
        showNotification(message);
      });

      FirebaseMessaging.onMessageOpenedApp.listen((RemoteMessage message) {
        FirebaseMessaging.onMessageOpenedApp.listen((RemoteMessage message) {
          log('A new onMessageOpenedApp event was published!');
          log('Message data: ${message.data}');
        });
      });
    }
  }

  void showNotification(RemoteMessage message) {
    RemoteNotification? notification = message.notification;
    if (notification != null) {
      flutterLocalNotificationsPlugin.show(
        notification.hashCode,
        notification.title,
        notification.body,
        const NotificationDetails(
            iOS: DarwinNotificationDetails(
              presentAlert: true,
              presentBadge: true,
              presentSound: true,
            ),
            android: AndroidNotificationDetails('1', 'pushnotification',
                channelDescription: 'Test', color: Color(0xffff9d89), colorized: true, priority: Priority.max, channelShowBadge: true, importance: Importance.high, playSound: true)),
      );
    }
  }
}
