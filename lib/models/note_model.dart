import 'dart:typed_data';

class NoteModel {
  final int? id;
  final String title;
  final String description;
  final double price;
  final Uint8List image;

  const NoteModel({required this.title, required this.description, this.price = 0, required this.image, this.id});

  /// OR
  /// factory NoteModel.fromJson(Map<String, dynamic> json) {
  ///     return NoteModel(
  ///       id: json['id'],
  ///       title: json['title'],
  ///       description: json['description'],
  ///     );
  ///   }

  factory NoteModel.fromJson(Map<String, dynamic> json) => NoteModel(
        id: json['id'],
        title: json['title'],
        description: json['description'],
        price: json['price'],
        image: json['image'],
      );

  /// OR
  /// Map<String,dynamic> toJson(){
  ///   return {
  ///     'id': id,
  ///     'title': title,
  ///     'description': description
  ///   };
  /// }

  Map<String, dynamic> toJson() => {
        'id': id,
        'title': title,
        'description': description,
        'price': price,
        'image': image,
      };
}
