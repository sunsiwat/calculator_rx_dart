import 'package:calculator_rx_dart/view_model/firebase_view_model.dart';
import 'package:calculator_rx_dart/view_model/map_view_model.dart';
import 'package:calculator_rx_dart/view_model/note_database_view_model.dart';
import 'package:calculator_rx_dart/view_model/syntax_view_model.dart';
import 'package:get_it/get_it.dart';

class AppDependencyInjection{
  /// private constructor
  AppDependencyInjection._internal();

  /// provides global point of access to class xxx
  static final AppDependencyInjection appInstance = AppDependencyInjection._internal();

  factory AppDependencyInjection(){
    return appInstance;
  }

  GetIt get getItInstance => GetIt.instance;

  void init(){
    _registerControllerDependency();
  }

  void _registerControllerDependency(){
    getItInstance.registerSingleton<SyntaxViewModel>(SyntaxViewModel());
    getItInstance.registerFactory<NoteDatabaseViewModel>(() => NoteDatabaseViewModel());
    getItInstance.registerFactory<FirebaseViewModel>(() => FirebaseViewModel());
    getItInstance.registerFactory(() => MapViewModel());
  }
}