import 'dart:async';
import 'dart:developer';

import 'package:calculator_rx_dart/core/app_dependency_injection.dart';
import 'package:calculator_rx_dart/view_model/map_view_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';

class GoogleMapPage extends StatefulWidget {
  const GoogleMapPage({super.key});

  @override
  State<GoogleMapPage> createState() => _GoogleMapPageState();
}

class _GoogleMapPageState extends State<GoogleMapPage> {
  late final MapViewModel mapViewModel;

  @override
  void initState() {
    mapViewModel = AppDependencyInjection.appInstance.getItInstance<MapViewModel>();
    mapViewModel.setInitialData();
    super.initState();
  }

  @override
  void dispose() {
    mapViewModel.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: const Text('GoogleMap'),
          actions: [
            IconButton(
              onPressed: () {
                mapViewModel.zoomOutBangkok();
              },
              icon: const Icon(Icons.home),
            ),
            IconButton(
              onPressed: () {
                mapViewModel.goToAirport();
              },
              icon: const Icon(Icons.airplanemode_on),
            ),
          ],
        ),
        body: StreamBuilder(
            stream: mapViewModel.dataCamObs,
            builder: (context, snapshot) {
              if (snapshot.connectionState == ConnectionState.waiting) {
                log('waiting');
                return const Center(child: CircularProgressIndicator());
              } else if (snapshot.hasData) {
                final CameraPosition getCamPosition = snapshot.data;
                return _buildMap(getCamPosition);
              } else {
                log('no data');
                return const Center(child: CircularProgressIndicator());
              }
            }),
        floatingActionButton: FloatingActionButton.extended(
          onPressed: () {
            mapViewModel.goToMyLocation();
          },
          label: const Text('My location'),
          icon: const Icon(Icons.near_me),
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.startFloat,
      ),
    );
  }

  Widget _buildMap(CameraPosition cameraPosition) {
    return StreamBuilder(
      stream: mapViewModel.dataMarkerObs,
      builder: (context, snapshot) {
        if(snapshot.hasData){
          return GoogleMap(
            myLocationEnabled: true,
            compassEnabled: true,
            markers: snapshot.data,
            mapType: MapType.normal,
            initialCameraPosition: cameraPosition,
            onMapCreated: (GoogleMapController controller) {
              mapViewModel.controller.complete(controller);
            },
            onTap: (LatLng latLng){
              mapViewModel.addMarker(latLng);
            },
          );
        }else{
          return const CircularProgressIndicator();
        }
      },
    );
  }
}
