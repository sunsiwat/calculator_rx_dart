import 'dart:developer';

import 'package:calculator_rx_dart/models/note_model.dart';
import 'package:calculator_rx_dart/screens/views/note_page.dart';
import 'package:calculator_rx_dart/screens/widgets/note_card.dart';
import 'package:calculator_rx_dart/view_model/note_database_view_model.dart';
import 'package:flutter/material.dart';

class ListNotePage extends StatefulWidget {
  final NoteDatabaseViewModel noteDatabaseViewModel;
  const ListNotePage({super.key, required this.noteDatabaseViewModel});

  @override
  State<ListNotePage> createState() => _ListNotePageState();
}

class _ListNotePageState extends State<ListNotePage> {
  late final NoteDatabaseViewModel noteDatabaseViewModel;
  @override
  void initState() {
    noteDatabaseViewModel = widget.noteDatabaseViewModel;
    noteDatabaseViewModel.initialData();
    super.initState();
  }

  @override
  void dispose() {
    // noteDatabaseViewModel.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.black,
        appBar: AppBar(
          title: const Text(
            'Database App',
          ),
          backgroundColor: Theme.of(context).colorScheme.inversePrimary,
        ),
        body: Padding(
          padding: const EdgeInsets.all(10.0),
          child: StreamBuilder(
            stream: noteDatabaseViewModel.dataNoteObs,
            builder: (context, snapshot) {
              if (snapshot.connectionState == ConnectionState.waiting) {
                return const CircularProgressIndicator();
              } else if (snapshot.hasData) {
                return ListView.separated(
                  itemBuilder: (context, index) {
                    return NoteCard(noteModel: snapshot.data[index], noteDatabaseViewModel: noteDatabaseViewModel);
                  },
                  separatorBuilder: (BuildContext context, int index) => const Divider(),
                  itemCount: snapshot.data.length,
                );
              } else {
                return const Center(
                  child: Text('No notes yet'),
                );
              }
            },
          ),
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            Navigator.push<void>(
              context,
              MaterialPageRoute<void>(
                builder: (BuildContext context) => NotePage(noteDatabaseViewModel: noteDatabaseViewModel),
              ),
            );
          },
          child: const Icon(Icons.add),
        ),
      ),
    );
  }
}
