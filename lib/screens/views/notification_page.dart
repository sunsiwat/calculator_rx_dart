import 'dart:developer';

import 'package:calculator_rx_dart/firebase_options.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';

class NotificationPage extends StatefulWidget {
  const NotificationPage({super.key});

  @override
  State<NotificationPage> createState() => _NotificationPageState();
}

class _NotificationPageState extends State<NotificationPage> {
  // final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();
  late final FirebaseMessaging _messaging;

  @override
  void initState() {
    setupFirebase();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Notification'),
      ),
      body: const Center(child: Text('data')),
    );
  }

  // @pragma('vm:entry-point')
  // Future<void> _firebaseMessagingBackgroundHandler(RemoteMessage message) async {
  //   // await Firebase.initializeApp();
  //   if(message.notification != null);
  //   {
  //     RemoteNotification? notification = message.notification;
  //     if(notification != null && notification.hashCode != null && notification.title != null && notification.body != null){
  //       flutterLocalNotificationsPlugin.show(
  //           notification.hashCode,
  //           notification.title,
  //           notification.body,
  //           const NotificationDetails(
  //               iOS: DarwinNotificationDetails(
  //                 presentAlert: true,
  //                 presentBadge: true,
  //                 presentSound: true,
  //               ),
  //               android: AndroidNotificationDetails('1', 'pushnotification',
  //                   channelDescription: 'Test', color: Color(0xffff9d89), colorized: true, priority: Priority.max, channelShowBadge: true, importance: Importance.high, playSound: true)));
  //       // print('Handling a background message: ${message.messageId}');
  //     }
  //   }
  //
  // }

  final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();

  // @pragma('vm:entry-point')
  // void notificationTapBackground(NotificationResponse notificationResponse) {
  //   // handle action
  //   FirebaseMessaging.onMessage.listen((RemoteMessage message) {
  //     RemoteNotification? notification = message.notification;
  //     if (notification != null) {
  //       final a = notification.hashCode;
  //       final b = notification.title;
  //       final c = notification.body;
  //       log('$a, $b, $c');
  //
  //       flutterLocalNotificationsPlugin.show(
  //           notification.hashCode,
  //           notification.title,
  //           notification.body,
  //           const NotificationDetails(
  //               iOS: DarwinNotificationDetails(
  //                 presentAlert: true,
  //                 presentBadge: true,
  //                 presentSound: true,
  //               ),
  //               android: AndroidNotificationDetails('1', 'pushnotification',
  //                   channelDescription: 'Test', color: Color(0xffff9d89), colorized: true, priority: Priority.max, channelShowBadge: true, importance: Importance.high, playSound: true)));
  //     }
  //   });
  // }

  void setupFirebase() async {
    await Firebase.initializeApp();
    _messaging = FirebaseMessaging.instance;
    String? token = await _messaging.getToken();
    log(token.toString());

    NotificationSettings settings = await _messaging.requestPermission(
      alert: true,
      badge: true,
      provisional: false,
      sound: true,
    );

    // await flutterLocalNotificationsPlugin.resolvePlatformSpecificImplementation<AndroidFlutterLocalNotificationsPlugin>()?.requestNotificationsPermission();

    if (settings.authorizationStatus == AuthorizationStatus.authorized) {
      // FirebaseMessaging.onBackgroundMessage(_firebaseMessagingBackgroundHandler);
      var androidInit = const AndroidInitializationSettings('@mipmap/ic_launcher');
      var iosInit = const DarwinInitializationSettings(
        requestAlertPermission: true,
        requestBadgePermission: true,
        requestSoundPermission: true,
      );

      var initializationSettings = InitializationSettings(android: androidInit, iOS: iosInit);

      flutterLocalNotificationsPlugin.initialize(
        initializationSettings,
      );

      FirebaseMessaging.onMessage.listen((RemoteMessage message) {
        log('receive message');
        RemoteNotification? notification = message.notification;
        if (notification != null) {
          final a = notification.hashCode;
          final b = notification.title;
          final c = notification.body;
          log('$a, $b, $c');

          flutterLocalNotificationsPlugin.show(
              notification.hashCode,
              notification.title,
              notification.body,
              const NotificationDetails(
                  iOS: DarwinNotificationDetails(
                    presentAlert: true,
                    presentBadge: true,
                    presentSound: true,
                  ),
                  android: AndroidNotificationDetails('1', 'pushnotification',
                      channelDescription: 'Test', color: Color(0xffff9d89), colorized: true, priority: Priority.max, channelShowBadge: true, importance: Importance.high, playSound: true)));
        }
      });

      FirebaseMessaging.onMessageOpenedApp.listen((RemoteMessage message) {
        FirebaseMessaging.onMessageOpenedApp.listen((RemoteMessage message) {
          log('A new onMessageOpenedApp event was published!');
          log('Message data: ${message.data}');
        });
      });
    }
  }
}
