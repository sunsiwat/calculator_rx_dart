import 'dart:developer';

import 'package:calculator_rx_dart/models/note_model.dart';
import 'package:calculator_rx_dart/view_model/note_database_view_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class NotePage extends StatefulWidget {
  final NoteModel? noteModel;
  final NoteDatabaseViewModel noteDatabaseViewModel;
  const NotePage({super.key, this.noteModel, required this.noteDatabaseViewModel});

  @override
  State<NotePage> createState() => _NotePageState();
}

class _NotePageState extends State<NotePage> {
  final titleController = TextEditingController();
  final descriptionController = TextEditingController();
  final priceController = TextEditingController();

  String titleText = 'Add a note';
  String buttonText = 'Save';
  bool saveOrEdit = false;

  void checkNoteIsEmpty() {
    if (widget.noteModel != null) {
      titleText = 'Edit Note';
      buttonText = 'Edit';
      titleController.text = widget.noteModel!.title;
      descriptionController.text = widget.noteModel!.description;
      priceController.text = widget.noteModel!.price.toString();
      saveOrEdit = true;
    }
  }

  @override
  Widget build(BuildContext context) {
    checkNoteIsEmpty();
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: Text(titleText),
          backgroundColor: Theme.of(context).colorScheme.inversePrimary,
        ),
        body: Padding(
          padding: const EdgeInsets.only(top: 20, bottom: 20),
          child: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const Text(
                  'What are you thinking about?',
                  style: TextStyle(fontSize: 20),
                ),
                SizedBox(
                  height: 20.h,
                ),
                titleField(),
                SizedBox(
                  height: 20.h,
                ),
                descriptionField(),
                priceField(),
                // const Spacer(),
                saveButton(context),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget titleField() {
    return Padding(
      padding: const EdgeInsets.all(10),
      child: TextFormField(
        controller: titleController,
        maxLines: 1,
        decoration: const InputDecoration(
          border: OutlineInputBorder(
              borderSide: BorderSide(
                width: 0.75,
                color: Colors.white,
              ),
              borderRadius: BorderRadius.all(Radius.circular(10))),
          labelText: 'Note Title',
          hintText: 'Title',
        ),
      ),
    );
  }

  Widget descriptionField() {
    return Padding(
      padding: const EdgeInsets.all(10),
      child: TextFormField(
        controller: descriptionController,
        maxLines: 5,
        decoration: const InputDecoration(
          border: OutlineInputBorder(
              borderSide: BorderSide(
                width: 0.75,
                color: Colors.white,
              ),
              borderRadius: BorderRadius.all(Radius.circular(10))),
          labelText: 'Note Description',
          hintText: 'Description',
        ),
      ),
    );
  }

  Widget priceField() {
    return Padding(
      padding: const EdgeInsets.all(10),
      child: TextFormField(
        controller: priceController,
        maxLines: 1,
        keyboardType: TextInputType.number,
        inputFormatters: <TextInputFormatter>[
          FilteringTextInputFormatter.allow(RegExp(r'^[+-]?\d+\.?\d*')),
          // RegExp(r'^\d+(\.\d)?$')
          // RegExp(r'^[+-]?(\d+)?\.?\d*')  --> verified
        ],
        decoration: const InputDecoration(
          border: OutlineInputBorder(
              borderSide: BorderSide(
                width: 0.75,
                color: Colors.white,
              ),
              borderRadius: BorderRadius.all(Radius.circular(10))),
          labelText: 'Note Description',
          hintText: 'Description',
        ),
      ),
    );
  }

  Widget saveButton(BuildContext context) {
    return InkWell(
      onTap: () async {
        log('id: ${widget.noteModel?.id} title: ${titleController.text}, decription: ${descriptionController.text}');
        final imageBytes = await widget.noteDatabaseViewModel.imageToUint8List();
        late NoteModel note;
        if (priceController.text == '') {
          note = NoteModel(title: titleController.text, description: descriptionController.text, image: imageBytes, id: widget.noteModel?.id);
        } else {
          log(priceController.text);
          note = NoteModel(title: titleController.text, description: descriptionController.text, price: double.parse(priceController.text), image: imageBytes, id: widget.noteModel?.id);
        }
        if (saveOrEdit) {
          await widget.noteDatabaseViewModel.updateNote(note);
        } else {
          if (titleController.text.isNotEmpty || descriptionController.text.isNotEmpty || priceController.text.isNotEmpty) {
            await widget.noteDatabaseViewModel.addNote(note);
          }
        }
        Navigator.pop(context);
        // await NoteDatabaseViewModel().addNote(note);
      },
      child: Container(
        padding: EdgeInsets.all(20),
        decoration: const BoxDecoration(color: Colors.blueAccent, borderRadius: BorderRadius.all(Radius.circular(10))),
        child: Text(buttonText),
      ),
    );
  }
}
