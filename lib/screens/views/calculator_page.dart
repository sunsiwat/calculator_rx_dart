import 'dart:developer';

import 'package:calculator_rx_dart/core/app_dependency_injection.dart';
import 'package:calculator_rx_dart/screens/widgets/button_num_pad.dart';
import 'package:calculator_rx_dart/view_model/syntax_view_model.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:math_expressions/math_expressions.dart';

class CalculatorPage extends StatefulWidget {
  // final SyntaxViewModel syntaxViewModel;
  // const CalculatorPage({super.key,required this.syntaxViewModel});
  const CalculatorPage({super.key,});

  @override
  State<CalculatorPage> createState() => _CalculatorPageState();
}

class _CalculatorPageState extends State<CalculatorPage> {
  late final SyntaxViewModel syntaxViewModel;

  @override
  void initState() {
    syntaxViewModel = AppDependencyInjection.appInstance.getItInstance<SyntaxViewModel>();
    syntaxViewModel.setInitialData();
    super.initState();
  }

  @override
  void dispose() {
    syntaxViewModel.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      appBar: AppBar(
        title: const Text('Calculator'),
        backgroundColor: Theme.of(context).colorScheme.inversePrimary,
      ),
      body: SafeArea(
        child: Column(
          children: [
            Expanded(
              flex: 1,
              child: _buildDisplaySyntax(context),
            ),
            Expanded(
              flex: 2,
              child: _buildNumPadWidget(context),
            )
          ],
        ),
      ),
    );
  }

  Widget _buildDisplaySyntax(BuildContext context) {
    return Container(
      width: double.infinity,
      padding: EdgeInsets.symmetric(vertical: 20.h, horizontal: 20.w),
      margin: EdgeInsets.symmetric(horizontal: 5.w, vertical: 5.h),
      decoration: BoxDecoration(borderRadius: BorderRadius.circular(20.0), color: Colors.yellow),
      child: StreamBuilder(
        stream: syntaxViewModel.dataStrObs,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            final cal = calculateExpression(snapshot.data);
            return Align(
              alignment: Alignment.topRight,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  Text(
                    snapshot.data.toString(),
                    style: TextStyle(fontSize: 25.sp),
                  ),
                  const SizedBox(
                    width: 20,
                  ),
                  Text(
                    cal == 'Error' ? '' : cal,
                    style: TextStyle(fontSize: 30.sp),
                  )
                ],
              ),
            );
          } else {
            // return CircularProgressIndicator();
            return const Text('');
          }
        },
      ),
    );
  }

  String calculateExpression(String expression) {
    try {
      Parser parser = Parser();
      Expression exp = parser.parse(expression);
      ContextModel cm = ContextModel();
      double result = exp.evaluate(EvaluationType.REAL, cm);
      return result.toString();
    } catch (e) {
      return 'Error';
    }
  }

  Widget _buildNumPadWidget(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Expanded(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              setButtonNumPad('c'),
              setButtonNumPad('<--'),
              setButtonNumPad('%'),
              setButtonNumPad('/'),
            ],
          ),
        ),
        Expanded(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              setButtonNumPad('7'),
              setButtonNumPad('8'),
              setButtonNumPad('9'),
              setButtonNumPad('*'),
            ],
          ),
        ),
        Expanded(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              setButtonNumPad('4'),
              setButtonNumPad('5'),
              setButtonNumPad('6'),
              setButtonNumPad('-'),
            ],
          ),
        ),
        Expanded(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              setButtonNumPad('1'),
              setButtonNumPad('2'),
              setButtonNumPad('3'),
              setButtonNumPad('+'),
            ],
          ),
        ),
        Expanded(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              setButtonNumPad('±'),
              setButtonNumPad('0'),
              setButtonNumPad('.'),
              setButtonNumPad('='),
            ],
          ),
        ),
      ],
    );
  }

  Widget setButtonNumPad(String char) {
    // return Expanded(child: ButtonNumPad(char: char, syntaxViewModel: syntaxViewModel));
    return Expanded(child: ButtonNumPad(char: char,));

  }
}
