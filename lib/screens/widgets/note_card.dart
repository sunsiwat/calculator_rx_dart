import 'package:calculator_rx_dart/models/note_model.dart';
import 'package:calculator_rx_dart/screens/views/note_page.dart';
import 'package:calculator_rx_dart/view_model/note_database_view_model.dart';
import 'package:flutter/material.dart';

class NoteCard extends StatelessWidget {
  final NoteModel noteModel;
  final NoteDatabaseViewModel noteDatabaseViewModel;
  const NoteCard({super.key, required this.noteModel, required this.noteDatabaseViewModel});

  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: Image.memory(noteModel.image),
      title: Text(noteModel.title),
      subtitle: Text(noteModel.description),
      trailing: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          Text(noteModel.price.toString()),
          IconButton(
            onPressed: () async {
              showDialog(
                  context: context,
                  builder: (BuildContext context) {
                    return showAlertDialog(context);
                  });
            },
            icon: const Icon(Icons.delete),
            color: Colors.red,
          ),
        ],
      ),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10),
        side: const BorderSide(color: Colors.grey, width: 1),
      ),
      tileColor: Colors.white,
      onTap: () => Navigator.push<void>(
        context,
        MaterialPageRoute<void>(
          builder: (BuildContext context) => NotePage(
            noteDatabaseViewModel: noteDatabaseViewModel,
            noteModel: noteModel,
          ),
        ),
      ),
    );
  }

  Widget showAlertDialog(BuildContext context) {
    return AlertDialog(
      title: const Text('Are you sure you want to delete this note?'),
      actions: [
        ElevatedButton(
          onPressed: () async {
            await noteDatabaseViewModel.deleteNote(noteModel);
            Navigator.pop(context);
          },
          child: const Text('Yes'),
        ),
        ElevatedButton(
          onPressed: () {
            Navigator.pop(context);
          },
          child: const Text('No'),
        ),
      ],
    );
  }
}
