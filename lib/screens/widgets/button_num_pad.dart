import 'package:calculator_rx_dart/core/app_dependency_injection.dart';
import 'package:calculator_rx_dart/view_model/syntax_view_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get_it/get_it.dart';

class ButtonNumPad extends StatelessWidget {
  final String char;
  // final SyntaxViewModel syntaxViewModel;
  final bool type;
  const ButtonNumPad({
    super.key,
    required this.char,
    // required this.syntaxViewModel,
    this.type = true,
  });

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        AppDependencyInjection.appInstance.getItInstance<SyntaxViewModel>().syntax(char);
        /// simple syntax --> call with GetIt
        // GetIt.instance<SyntaxViewModel>().syntax(char);
        ///
        // syntaxViewModel.syntax(char);
      },
      child: Container(
        margin: EdgeInsets.all(2.h),
        decoration: BoxDecoration(
          color: Colors.grey.shade900,
          shape: BoxShape.circle,
        ),
        child: Center(
            child: Text(
          char,
          style: TextStyle(
            fontSize: 25.sp,
            color: type ? Colors.blueAccent : Colors.purple,
          ),
        )),
      ),
    );
  }
}
